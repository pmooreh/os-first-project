#include "stdint.h"
#include "thread.h"
#include "console.h"

#define N 4

typedef struct Thread {
} Thread;

typedef struct Queue {
    Thread *first;
    Thread *last;
} Queue;

static Queue ready;

static Thread threads[N];
static Thread* activeThread;

void threadInit(void) {
    for (int i=0; i<N; i++) {
    }
    activeThread = &threads[0];
    ready.first = 0;
    ready.last = 0;
}

int threadId(void) {
    return 0;
}

void threadYield(void) {
}

int threadCreate(ThreadFunc func, void* arg) {
    return 0;
}

void threadExit(void) {
}
