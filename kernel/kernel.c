#include "stdint.h"
#include "console.h"
#include "gdt.h"
#include "thread.h"

volatile int zztop = 0;

void yld() {
    sayDec("yielding",zztop++);
    threadYield();
    sayDec("we're back",zztop++);
}

int count0 = 0;

void g0(void* arg) {
    say("g0");    
    say("exiting");
    threadExit();
    putStr("***** The impossible has happened\n");
}

void h0(void* arg) {
    char* s = (char*) arg;
    say ("h0");
    say(s);
    say("returning");
}

void f0(void* arg) {
    sayDec("f0_0",count0);
    threadYield();
    sayDec("f0_1",count0);
    threadYield();
    sayDec("f0_2",count0);
    threadExit();
}

void f1(void* arg) {
    char* s = (char*)arg;

    if (s == 0) {
        say("null arg");
        yld();
        int x = zztop + 2;
        yld();
        if (zztop == x) {
            say("yielding to myself");
        }
        say("I guess I have to exit");
        threadExit();
    }

    say("f1");
    yld();
    say(s);
    for (int i=0; i<5; i++) {
        sayDec(s,i);
        yld();
    }
    say("creating thread inside thread");
    int id = threadCreate(f1,0);
    sayDec("id",id);
    yld();
}

void f2(void* arg) {
    char* s = (char*)arg;

    say(s);
    yld();
    say("f2");
    yld();
    say("f2, again");
}

void kernelMain(void) {
    putStr("\nstarting kernel\n");

    putStr("initializing threads\n");
    threadInit();

    say("main thread");

    say("creating g0");
    threadCreate(g0,"hello");
    threadYield();

    say("creating h0");
    threadCreate(h0,"hello");
    threadYield();
    
    say("creating f0");
    int child = threadCreate(f0,"hello");
    sayDec("child id",child);

    sayDec("yielding to child", count0);
    threadYield();
    sayDec("child came back, yielding again",count0);
    threadYield();
    sayDec("child came back, yielding one more time",count0);
    threadYield();
    sayDec("... and another",count0);
    threadYield();
    sayDec("... even more",count0);

    say("creating child");
    child = threadCreate(f1,"first child");
    sayDec("child id",child);

    yld();

    yld();

    say("creating child2");
    int child2 = threadCreate(f2,"c2");
    sayDec("created",child2);
    say("creating child3");
    int child3 = threadCreate(f2,"c3");
    sayDec("created",child3);
    say("creating child4");
    int child4 = threadCreate(f2,"c4");
    sayDec("created",child4);
    yld();
    say("main about to exit");
    threadExit();

    putStr("*** should never see this\n");

    shutdown();
}
